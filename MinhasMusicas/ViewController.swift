//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit
struct Musica {
    let nomeMusica:String
    let nomeAlbum:String
    let nomeCantor:String
    let nomeImagemPequena:String
    let nomeImagemGrande:String
}
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var listaMusica:[Musica]=[]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Musicas", for: indexPath) as! MyCell
        let musica = self.listaMusica[indexPath.row]
        cell.Musica.text =  musica.nomeMusica
        cell.Album.text = musica.nomeAlbum
        cell.Cantor.text = musica.nomeCantor
        cell.Capa.image = UIImage(named: musica.nomeImagemPequena)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "AbrirDetalhe", sender: indexPath.row)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detahesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaMusica[indice]
        detahesViewController.nomeMusica = musica.nomeMusica
        detahesViewController.nomeAlbum = musica.nomeAlbum
        detahesViewController.nomeCantor = musica.nomeCantor
        detahesViewController.nomeImage = musica.nomeImagemGrande
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        self.listaMusica.append(Musica(nomeMusica: "Pontos Cardeais", nomeAlbum: "Álbum Vivo!", nomeCantor: "Alceu Valença", nomeImagemPequena: "capa_alceu_pequeno", nomeImagemGrande: "capa_alceu_grande"))
        self.listaMusica.append(Musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Álbum Patota de Cosme", nomeCantor: "Zeca Pagodinho", nomeImagemPequena: "capa_zeca_pequeno", nomeImagemGrande: "capa_zeca_grande"))
        self.listaMusica.append(Musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", nomeImagemPequena: "capa_adoniran_pequeno", nomeImagemGrande: "capa_adoniran_grande"))
    }

    
    
}

